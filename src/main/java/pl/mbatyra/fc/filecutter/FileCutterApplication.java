package pl.mbatyra.fc.filecutter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FileCutterApplication {

    public static void main(String[] args) {
        SpringApplication.run(FileCutterApplication.class, args);
    }

}

